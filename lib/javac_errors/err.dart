/// Base class for all type of compilation errors.
///
/// Reference: https://introcs.cs.princeton.edu/java/11cheatsheet/errors.pdf
abstract class Err {}
